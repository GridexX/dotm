const setData = (project, date, time, description = "") => {
    const hashtags = description.split` `.filter((s) => s.startsWith`#`);

    return {
        project: project,
        time_entries: [
            {
                date: date,
                time: time,
                description: description,
                hashtags: hashtags,
            },
        ],
    };
};

module.exports = {
    setData,
};
