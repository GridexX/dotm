const {
    getTimeEachProject,
    insertTimeEntry,
    getTimePerDay,
    getTimeHash,
    reportPerDay,
    projectNameExists,
    listProjects,
} = require("../dao/time_entryDAO.js");

const { formatTime, advancedPattern } = require("./time_operations.js");

const { setData } = require("./data_operations.js");

const printHelp = () => {
    console.log("DOTM is a little utility to time manage your projects");
    console.log(
        "Options: [time] [project] [description] : add for a new project a time entry and an optionnal description"
    );
    console.log(
        "dotm GET [time] : Return for each project, the total amount of time"
    );
    console.log(
        "dotm GET-HASH [hashtag] [project] : Return the total amount of time on the hashtag, and optionnaly the project"
    );
    console.log(
        "dotm REPORT PERDAY [project] : Return for each day (where there is a time entry) the total amount of time that has been spent"
    );
};

const getAndPrintInfoProject = async (time_entries, projectName) => {
    const projectExistence = await projectNameExists(time_entries, projectName);
    if (!projectExistence) {
        console.log(
            `Project ${projectName} doesn't exists, please specify a valid project name amoung :`
        );
        const validProjects = await listProjects(time_entries);
        console.log(validProjects.map((e) => e.project));
        return null;
    }
    return projectExistence;
};
/*
 * After getting the time for each project, this fonction
 * parse the result in a more conventionnal way to print it to the console
 */
const parseTimeEntries = (arrayOfTime, isPerDay = false) => {
    if (arrayOfTime.length === 1) {
        return arrayOfTime[0].totalTime / 60 + "h";
    } else
        return arrayOfTime.map((e) => {
            if (!isPerDay)
                return {
                    project: e._id,
                    timeTotal: e.totalTime / 60 + "h",
                };
            else
                return {
                    date: e._id,
                    timeTotal: e.totalTime / 60 + "h",
                };
        });
};

const getProject = async (time_entries, option1, option2) => {
    let results = null;
    if (!option1) {
        console.log(
            "Not project specified ! It will return total time for each project :"
        );
        results = await getTimeEachProject(time_entries);

        //Get project without a specified date
    } else if (option1) {
        const projectName = option2 ? option2 : null;

        let date = option1;
        //Add a unused basic Pattern and retrieve only the date
        try {
            date = advancedPattern(option1 + " 1h")[0];
        } catch (e) {
            console.log("Advanced pattern not found...");
            return null;
        }

        if (projectName) {
            results = await getAndPrintInfoProject(time_entries, projectName);
            if (!results) return results;
        }

        const projectNameStr = projectName
            ? "on project " + projectName + " :"
            : " :";

        results = await getTimePerDay(time_entries, date, projectName);
        try {
            const [{ _id }] = results;
            console.log(
                `Total amount of time at date ${date} ${projectNameStr}`
            );
        } catch (e) {
            console.log(`No time entry for ${date}`);
            return null;
        }
        return parseTimeEntries(results);
    } else {
        console.log("Invalid request format, please check the doc !");
    }
    return results;
};

const getHash = async (time_entries, hashtag, project) => {
    if (!hashtag) {
        console.log("You must specify a hashtag as parameter");
        return null;
    }
    let projectName = project ? "on project " + project + " :" : " :";
    hashtag = hashtag.startsWith("#") ? hashtag : "#" + hashtag;
    const results = await getTimeHash(time_entries, hashtag, project);
    try {
        const [{ _id }] = results;
        console.log(
            `Total amount of time on hashtag ${hashtag} ${projectName}`
        );
    } catch (e) {
        console.log(`No time entry for #${hashtag}`);
        return null;
    }

    return parseTimeEntries(results);
};

const getReportPerDay = async (time_entries, project) => {
    let results = null;

    if (!project) {
        console.log("Not project specified ! You must specify a project name.");
        return null;
    } else {
        results = await getAndPrintInfoProject(time_entries, project);
        if (!results) return results;
    }

    console.log(`Amount of time that have been spent on ${project} :`);
    results = await reportPerDay(time_entries, project);
    return parseTimeEntries(results, true);
};

const addTimeEntry = async (
    time_entries,
    timePattern,
    project,
    description
) => {
    let projectExistence = false;
    if (!project) {
        console.log(
            "Not project specified ! You must specify a project name !"
        );
        return null;
    } else {
        projectExistence = await projectNameExists(time_entries, project);
        if (!projectExistence) {
            console.log(`Creating a new ${project} project...`);
        } else {
            console.log(
                `Project already exists, adding time entry into ${project}...`
            );
        }
    }

    if (!description) {
        console.log("No description specified, it will be empty");
    }

    //Retrieve the data
    try {
        const [date, minutes] = formatTime(timePattern);
        const data = setData(project, date, minutes, description);
        console.log("Inserting a new time entry in the DB...");
        await insertTimeEntry(time_entries, data);
        console.log("Time entry successfully inserted :");
        return data;
    } catch (e) {
        console.log(e.message);
    }
    return null;
};

async function parseArguments(args, time_entries) {
    const [, , cmd, option1, option2] = args;

    if (!cmd) {
        console.log(
            "No arguments defined please use help or --h option to show all available commands."
        );
        return null;
    }

    if (cmd === "help" || cmd === "--h") {
        printHelp();
        return null;
    } else if (cmd === "GET") {
        return await getProject(time_entries, option1, option2);
    } else if (cmd === "GET-HASH") {
        return await getHash(time_entries, option1, option2);
    } else if (cmd === "REPORT" && option1 === "PERDAY") {
        // description is use on the project
        return await getReportPerDay(time_entries, option2);
    } else {
        //Time command
        //Retrieve the description if not quoted in a single args
        let description = option2 ? option2 : "";
        if (option2 && args.length > 5) {
            description = args.splice(4, args.length - 1).join(" ");
        }
        return addTimeEntry(time_entries, cmd, option1, description);
    }
}

module.exports = {
    parseArguments,
    addTimeEntry,
    getProject,
    getHash,
    getReportPerDay,
};
