const { DateTime } = require("luxon");

//Used for WeekdDay and Month Patterns
const weekDayLongs = [1, 2, 3, 4, 5, 6, 7].map(
    (e) => DateTime.local(2022, 1, e).weekdayLong
);
const weekDayShorts = weekDayLongs.map((e) =>
    e !== "Thursday" ? e.slice(0, 3) : e.slice(0, 4)
);
const monthLongs = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(
    (e) => DateTime.local(2022, e, 1).monthLong
);
const monthShorts = monthLongs.map((e) => e.slice(0, 3));

function roundFifteen(minute) {
    return parseInt(Math.round(minute / 15) * 15);
}

/*
 * Convert a time entry of a simple pattern to minutes
 * @param : time is a string and the separator between hours and minutes
 * Round minutes of fifteen
 * @return 2h30 => 150
 */
const simpleEntryToMinutes = (time, sep) => {
    return roundFifteen(
        time
            .split(sep)
            .map((e, i, a) =>
                i === 0 && a.length > 1
                    ? e * 60
                    : sep === "." || sep === ","
                    ? parseFloat(`0.${e}`) * 60
                    : parseInt(e)
            )
            .reduce((a, b) => a + b)
    );
};

/*
 * Use simpleEntryToMinutes and manage specific cases

 */

const basicTimeToMinutes = (time) => {
    if (time.endsWith("h")) {
        time += "0";
    } else if (
        !time.includes("m") &&
        !time.includes("h") &&
        !time.includes(":")
    ) {
        return roundFifteen(time);
    }
    return simpleEntryToMinutes(
        time.replace("m", ""),
        time.includes(":") ? ":" : "h"
    );
};

/*
 * Convert a pattern of range of hours to Minutes
 */
const rangeHoursToMinutes = (time, separator = "-", containsAmOrPm = true) =>
    Math.abs(
        time
            .split(separator)
            .map((e, i) => {
                let add = !containsAmOrPm && i === 0 ? 0 : 12;
                add = e.includes("a") ? 0 : add;
                return parseInt(e) + add;
            })
            .reduce((a, b) => b - a) * 60
    );

/*
 * Match simple pattern of arguments with no date inputs
 * Ex : 2h30, 2-4, 4am to 5pm
 */
const basicPattern = (time, returnError = true) => {
    /*
     * Match all patterns with a comma or dot delimiataton
     * .1
     * 0,9
     *
     */
    if (/^\d*(\.|,)\d+$/.test(time)) {
        if (time.startsWith(".") || time.startsWith(",")) time = "0" + time;

        return simpleEntryToMinutes(time, time.includes(",") ? "," : ".");

        /*
         * Match all hours and minute without convertion patterns :
         * 15
         * 15m
         * 2h
         * 2h30
         * 2h30m
         * 2:30
         *
         */
    } else if (/^\d+(h|m|:)\d*m*$/.test(time) || !isNaN(time)) {
        return basicTimeToMinutes(time);

        /*
         * Match all patterns in a time range without specifing am or pm
         * 4 to 9
         * 4-9
         */
    } else if (/^\d+(-|\ to\ )\d+$/.test(time)) {
        const separator = time.includes(" to ") ? " to " : "-";

        return rangeHoursToMinutes(time, separator, false);
        /*
         * Match all patterns in a time range specifing am or pm
         * 4am-6pm
         */
    } else if (/^\d+(a|p)m-\d+(a|p)m$/.test(time)) {
        return rangeHoursToMinutes(time, "-", true);
    } else if (returnError) {
        console.log(
            "Basic pattern not recognized, please read the doc to found all matching patterns"
        );
    }
    return null;
};

const isWeekDayPattern = (maybeWeekdayString) =>
    weekDayShorts.indexOf(maybeWeekdayString) !== -1 ||
    weekDayLongs.indexOf(maybeWeekdayString) !== -1;

const getDateFromWeekday = (weekDayLong) => {
    const weekday = weekDayLong.slice(0, 3);
    let i = 0;
    while (DateTime.now().minus({ days: i }).weekdayShort !== weekday) ++i;
    return DateTime.now().minus({ days: i }).toLocaleString();
};

const parseWeekDayPattern = (weekDayLong, parsedtimePattern) => {
    const date = getDateFromWeekday(weekDayLong);
    return [date, parsedtimePattern];
};

const isMonthPattern = (maybeMonthPattern) =>
    monthShorts.indexOf(maybeMonthPattern) !== -1 ||
    monthLongs.indexOf(maybeMonthPattern) !== -1;

const parseMonthPattern = (monthNameLong, dayNumber, parsedtimePattern) => {
    const monthNameShort = monthNameLong.slice(0, 3);
    let i = 0;
    while (DateTime.now().minus({ months: i }).monthShort !== monthNameShort)
        ++i;

    //Calculate the old year if it's a past month
    let months = DateTime.now().minus({ months: i }).month;
    let yearDecal = DateTime.now().month < months ? 1 : 0;
    let year = DateTime.now().minus({ year: yearDecal }).year;

    return [
        DateTime.local(year, months, parseInt(dayNumber)).toLocaleString(),
        parsedtimePattern,
    ];
};

const resetTimePattern = (timeArrayPattern, isWeekday = true) => {
    const ind = isWeekday ? 0 : 1;
    timeArrayPattern[ind] = timeArrayPattern[ind] + "|";
    const timeRearrange = timeArrayPattern.join(" ").split("|")[1].slice(1);
    return timeRearrange;
};

const parseSlashedDate = (time) => {
    let [date, hours] = time
        .replace(", ", "|")
        .replace(",", "|")
        .replace(" ", "|").split`|`;
    const parsedHours = basicPattern(hours);
    if (!parsedHours) {
        return null;
    }

    const splitDate = date.split`/`.map((e) => parseInt(e));
    date = splitDate.join`/`;
    if (splitDate.length === 2) {
        //Assign good year if date is in the past
        const month = parseInt(splitDate[0]);
        const day = parseInt(splitDate[1]);
        const currentMonth = DateTime.now().month;
        const currentDay = DateTime.now().day;
        const minusYear =
            month > currentMonth || (month >= currentMonth && day > currentDay)
                ? 1
                : 0;
        date = `${date}/${DateTime.now().minus({ year: minusYear }).year}`;
    }

    return [date, basicPattern(hours)];
};

//Match advanced patterns for the date
const advancedPattern = (time) => {
    /*
     * Match pattern with yesterday or y + basicPattern
     *
     */
    if (/^(y|yesterday)\ .*$/.test(time)) {
        const hours = basicPattern(time.replace(" ", "|").split`|`[1]);
        if (!hours) {
            return null;
        }
        return [DateTime.now().minus({ days: 1 }).toLocaleString(), hours];
        /*
         * Match to range pattern without final hour
         * 4-
         * 4 to
         */
    } else if (/^\d+(-|\ to\ ?)$/.test(time)) {
        let hourPattern = `${time.replace(" to", "-")}${DateTime.now().hour}`;
        return [DateTime.now().toLocaleString(), basicPattern(hourPattern)];
    } else if (/^\d?\d\/\d?\d(\/(\d){4})?(,)?.*$/.test(time)) {
        /* Match with date in format mm/dd, basicPattern or mm/dd/yyyy, basicPattern
         * 12/31, 4am to 6pm
         * 1/15/2021,23
         */

        return parseSlashedDate(time);
    } else if (/^\w{1,8},?\ ?.*$/.test(time)) {
        /* Match Abreviated and plain form of month and weekday pattern plus basiPattern
         *
         */
        let listArgs = time.replace(", ", " ").replace(",", " ").split` `;
        let [date, dayNumberIfMonthPattern] = listArgs;
        if (isWeekDayPattern(date)) {
            const parsedtimePattern = basicPattern(
                resetTimePattern(listArgs, true)
            );
            if (!parsedtimePattern) {
                return null;
            }
            return parseWeekDayPattern(date, parsedtimePattern);
        } else if (isMonthPattern(date)) {
            const parsedtimePattern = basicPattern(
                resetTimePattern(listArgs, false)
            );
            if (!parsedtimePattern) {
                return null;
            }
            return parseMonthPattern(
                date,
                dayNumberIfMonthPattern,
                parsedtimePattern
            );
        }
    } else {
        console.log(
            "Advanced pattern not recognized, please read the doc to found all matching patterns"
        );
        return false;
    }
};

//Return a tuple date + minute passed on project
const formatTime = (time) => {
    const date = DateTime.now().toLocaleString();
    const calculatedHours = basicPattern(time, false);

    if (isNaN(calculatedHours) || !calculatedHours) {
        return advancedPattern(time);
    } else return [date, calculatedHours];
};

module.exports = {
    basicPattern,
    advancedPattern,
    formatTime,
};
