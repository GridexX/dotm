# DOTM

Dotm is a little project of time manager. It was made during the S5 of my formation in DO at Polytech Montpellier to apply skills learned during WAO module in MongoDB and JavaScript.

## Setup the project
To use this project, [Node.js](https://nodejs.org) and MongoDB are required. To simply use a self-hosted [MongoDB](https://mongodb.com/) instance, I suggests using docker with [bitnami/mongodb](https://hub.docker.com/r/bitnami/mongodb) image.  

**Steps :**
1. Clone the repository in a directory
1. Launch the docker image   
    To have a persistence database, you have to specify a mountpoint with `-v` option.  
    We will use `opt/data/mongodb`.  
    Authorise the docker user on this directory with `chmod -R 1001 /opt/data/mongodb`  
    👉 Specify a custom `MONGODB_ROOT_PASSWORD` and a port (default is `27017`)
    Finally lauch the container with the following :
    ```bash
    docker run -d -v /opt/data/mongodb:/bitnami/mongodb -e MONGODB_ROOT_PASSWORD='password' -p 27017:27017 bitnami/mongodb:latest```
1. Modify the variable in the `.env` file, with your credentials.
1. Launch the script and DOTM 🚀

## Fonctionnality

DOTM is a simple cli-like where you setup and retrieve time entries for differents projects.

### Set a new time entry

To insert a new time entry on a project, here is one basic command :
```bash
node dotm.js "15m" mongodb "Working on #mongoDB for #woa course" 
```

The syntax is :
```bash
node dotm.js [time] [project] [description]
``` 
#### Time entry

DOTM makes intelligent guesses, but when you need to you can override them easily by adding m, or h to your entry.

Entry are made of two patterns :

##### Basic patterns :
Those patterns simply describe a amount of time in minutes, hours or range of hours.

| **Basic Pattern Entry** | **DOTM translation** |
| ----| ---- |
| 15 | 15 minutes |
| 15m | 15 minutes |
| 15h | 15 hours |
| 13 | 15 minutes* |
| 9-5 | 8 hours (9am to 5pm) |
|2pm-4pm | 2 hours |
| 10 to | 5 hours ("10am till now", assuming it's 3pm now) |
| 9- | 6 hours ("10am till now", assuming it's 3pm now)|
| 1:15 | 1 hours, 15 minutes |
| 1,17 | 1 hours, 15 minutes* |
| 3.5 | 3 hours, 30 minutes |
| 2.75 | 2 hours, 45 minutes |

ℹ️ * DOTM rounds minutes to fifteen, like default is the industry standard of 15 minutes.  

When used, basic patterns will be saved as the date of today.

You can dive deeper into time manipulation by using Advanced patterns. With them you can specify a time entry at a specific date. 
Here is an example :
```bash
node dotm.js "1/1/2017, 2h15m" mongodb "Working on #mongoDB for #woa course" 
```

This command will add in the project MongoDB an new new time entry on the first January of 2017. You can combine advanced patterns with basic into your command. Here is the list of them : 

| **Advanced Pattern Entry** | **DOTM translation** |
| ----| ---- |
|yesterday 14| 14h on yesterday's date|
|y 2,75| 2 hours 45 minutes on yesterday's date|
| Thursday, 2.5 | 2 hours 30 minutes on the most recent Thursday |
|  Wed 4 | 4 hours on the most recent Wednesday     |
| Oct 21, .5 | 30 minutes on October 21 |
| December 12 2h | 2h on December 12|
| 2/5, 2pm-4pm | 2 hours on February 5 |
| 1/1/2017, 2h13m | 2 hours, 15 minutes on January 1, 2017 |


**How it works ?**  
Basic pattern are tested before advanced patters. If one match, it is translate into a couple [date, hour], with today's date. If no one match advanced pattern are tested. They are split frop basic pattern with comma or space separaror : `advancedPattern(,| )basicPattern`.   
The advancedPattern get the hours from the basic on modify the date according to the pattern entry.


#### Description

Description is optionnal. The best descriptions are short and to the point, and make use of tags so it's easier to find and group entries later.  

#### Tag

A **tag** is a word with a hash sign (#) at the beginning.
E.g: `Working on my #homework #mongodb`, would had two tags to this entry: `homework` and `mongodb`.


### Reports 
With those commands, you can have a quick look on your time entry on make request depending on the date or the project.

Sumup all time entries for the time specified on the project.   
time is an advancedPattern without the hour specified.  
*Example: `December 12 2h` => `December 12`*
```bash
node dotm.js GET [time] [project]
```

Return for each project the sum of all time entries on the specified date : 
```bash
node dotm.js GET [time] 
```

Return for each project the sum of all time entries : 
```bash
node dotm.js GET
```

Return the sum of all time entries on a specific hashtag : 
```bash
node dotm.js GET-HASH [hashtag]
```

Return the sum of all time entries on a specific hashtag on a project : 
```bash
node dotm.js GET-HASH [hashtag] [project]
```

Return for each day (where there is a time entry) the total amount of time that has been spent on a project : 
```bash
node dotm.js REPORT PERDAY [project]
```

## Packages
DOTM is based on `Node.js`. Some packages were used on this project :  
- [luxon](https://moment.github.io/luxon/#/) was used for time conversions
- [jest](https://jestjs.io/) was used for Unit tests.

## Database Model

Here you can find a representation of the database model : https://drive.google.com/file/d/1apsDvX472j7k3zUEyl_RpM5tJ1gN7tts/view?usp=sharing

---
Made by @GridexX  
January 2022