const { MongoClient } = require("mongodb");
const { parseArguments } = require("./utils/arguments_parser.js");

MongoClient.connect(process.env.DB_URI, { useNewUrlParser: true })
    .catch((err) => {
        console.error(err.message);
        //process.exit(1);
    })
    .then(async (client) => {
        //console.log("Connection to the DB established");
        const time_entries = await client.db("dotm").collection("time_entries");
        const results = await parseArguments(process.argv, time_entries);
        if (results) console.log(results);
        await client.close();
    });
