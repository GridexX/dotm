const reportPerDay = async (time_entries, project) => {
    const pipeline = [
        { $match: { project: project } },
        { $unwind: { path: "$time_entries" } },
        {
            $group: {
                _id: "$time_entries.date",
                totalTime: { $sum: "$time_entries.time" },
            },
        },
    ];

    const results = await time_entries.aggregate(pipeline).toArray();
    return results;
};

const getTimeHash = async (time_entries, hashtag, project = "") => {
    const pipeline = [
        { $unwind: { path: "$time_entries" } },
        { $match: { $expr: { $in: [hashtag, "$time_entries.hashtags"] } } },
        { $group: { _id: hashtag, totalTime: { $sum: "$time_entries.time" } } },
    ];

    if (project.length > 0) {
        pipeline.unshift({ $match: { project: project } });
    }

    const results = await time_entries.aggregate(pipeline).toArray();
    return results;
};

const getTimePerDay = async (time_entries, date, project = "") => {
    const pipeline = [
        { $unwind: { path: "$time_entries" } },
        { $match: { $expr: { $eq: [date, "$time_entries.date"] } } },
        {
            $group: {
                _id: "$project",
                totalTime: { $sum: "$time_entries.time" },
            },
        },
    ];

    if (project) {
        pipeline.unshift({ $match: { project: project } });
    }

    const results = await time_entries.aggregate(pipeline).toArray();
    return results;
};

const getTimeEachProject = async (time_entries, project = "") => {
    const pipeline = [
        {
            $project: {
                _id: "$project",
                totalTime: { $sum: "$time_entries.time" },
            },
        },
    ];

    if (project.length > 0) {
        pipeline.unshift({ $match: { project: project } });
    }

    const results = await time_entries.aggregate(pipeline).toArray();
    return results;
};

const insertTimeEntry = async (time_entries, data) => {
    let res = await time_entries.findOne({ project: data.project });
    if (res === null) {
        res = await time_entries.insertOne(data);
    } else {
        res = await time_entries.updateMany(
            { project: data.project },
            { $addToSet: { time_entries: data.time_entries[0] } }
        );
    }

    return res;
};

const projectNameExists = async (time_entries, projectName) => {
    const nbProject = await time_entries.find({ project: projectName }).count();
    return nbProject > 0;
};

const listProjects = async (time_entries) => {
    return await time_entries.find().toArray();
};

module.exports = {
    reportPerDay,
    getTimeHash,
    getTimePerDay,
    getTimeEachProject,
    insertTimeEntry,
    projectNameExists,
    listProjects,
};
