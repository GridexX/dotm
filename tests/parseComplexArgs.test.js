const { advancedPattern } = require("../utils/time_operations");
const { DateTime } = require("luxon");

describe("Parse corectly past date with yesterday", () => {
    const yesterday = DateTime.now().minus({ days: 1 }).toLocaleString();

    test("2 hours for yesterday with abrevation", () => {
        const minutes = 120;
        expect(advancedPattern("y 2h")).toStrictEqual([yesterday, minutes]);
    });
    test("2 hours for yesterday", () => {
        const minutes = 120;
        expect(advancedPattern("yesterday 2h")).toStrictEqual([
            yesterday,
            minutes,
        ]);
    });
    test("Basic patterns with yesterday 1/3", () => {
        const minutes = 105;
        expect(advancedPattern("yesterday 1h38m")).toStrictEqual([
            yesterday,
            minutes,
        ]);
    });
    test("Basic patterns with yesterday 2/3", () => {
        const minutes = 8 * 60;
        expect(advancedPattern("yesterday 9-5")).toStrictEqual([
            yesterday,
            minutes,
        ]);
    });
    test("Basic patterns with yesterday 3/3", () => {
        const minutes = 120 + 45;
        expect(advancedPattern("yesterday 2.75")).toStrictEqual([
            yesterday,
            minutes,
        ]);
    });
});

describe("Parse correctly date with months and day", () => {
    test("Basic Month and day pattern", () => {
        expect(advancedPattern("1/1/2017, 2h13m")).toStrictEqual([
            "1/1/2017",
            2 * 60 + 15,
        ]);
    });

    test("Wipe zeros on month and day, ignore comma missing", () => {
        const currentYear = DateTime.now().year;
        const parsedPattern = advancedPattern("02/05 2pm-4pm");
        expect(parsedPattern).toContain(2 * 60);
        expect(parsedPattern[0]).toMatch(/2\/5\/20\d\d/);
    });

    test("Match month string and day number", () => {
        const parsedPattern = advancedPattern("December 12 2h");
        expect(parsedPattern).toContain(2 * 60);
        expect(parsedPattern[0]).toMatch(/12\/12\/20\d\d/);
    });

    test("Match abrevated month string and day number", () => {
        const parsedPattern = advancedPattern("Oct 21, .5");
        expect(parsedPattern).toContain(30);
        expect(parsedPattern[0]).toMatch(/10\/21\/20\d\d/);
    });
});
