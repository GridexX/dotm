const { basicPattern } = require("../utils/time_operations");

describe("Basic operations, simple time convertions to minutes", () => {
    test("Expect output as a number", () => {
        expect(basicPattern("15")).toBe(15);
    });
    test("15m renders 15", () => {
        expect(basicPattern("15m")).toBe(15);
    });
    test("1h converts to 60 minutes", () => {
        expect(basicPattern("1h")).toBe(60);
    });
    test("2h30 converts to 150 minutes", () => {
        expect(basicPattern("2h30")).toBe(150);
    });
    test("1h30m, remove m and converts to 150 minutes", () => {
        expect(basicPattern("1h30")).toBe(90);
    });
});

describe("Rounding operations of minutes", () => {
    test("7 rounds to 0", () => {
        expect(basicPattern("7")).toBe(0);
    });
    test("8 rounds to 15", () => {
        expect(basicPattern("8")).toBe(15);
    });
    test("3h10 converts and rounds to 195 minutes", () => {
        expect(basicPattern("3h10")).toBe(195);
    });
    test("1:38 converts and rounds to 105 minutes", () => {
        expect(basicPattern("1:38")).toBe(105);
    });
    test("1h24m remove m, converts and rounds to 90 minutes", () => {
        expect(basicPattern("1h24m")).toBe(90);
    });
    test("Dot converts correctly", () => {
        expect(basicPattern(".5")).toBe(30);
    });
    test("Comma converts correctly", () => {
        expect(basicPattern("0,5")).toBe(30);
    });
    test("Dot conversion and round to fifteen", () => {
        expect(basicPattern("1.17")).toBe(75);
    });
});

describe("Parse corectly range of hours", () => {
    test("Range 9-5 returns 8 hours", () => {
        expect(basicPattern("9-5")).toBe(8 * 60);
    });
    test("Test pm to pm", () => {
        expect(basicPattern("2pm-4pm")).toBe(120);
    });
    test("Test am to pm", () => {
        expect(basicPattern("2am-4pm")).toBe(14 * 60);
    });
    test("Test am to am", () => {
        expect(basicPattern("2am-5am")).toBe(180);
    });
    test("Test pm to am (during the night)", () => {
        expect(basicPattern("9pm-9am")).toBe(12 * 60);
    });
    test("Range 9-5 returns 8 hours", () => {
        expect(basicPattern("9-5")).toBe(8 * 60);
    });
});

// describe("Basic pattern not recognized", () => {
//     test("");
// });
