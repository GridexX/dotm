const { MongoClient } = require("mongodb");
const {
    addTimeEntry,
    getProject,
    getHash,
    getReportPerDay,
} = require("../utils/arguments_parser");
const { setData } = require("../utils/data_operations");
const { basicPattern } = require("../utils/time_operations");

describe("Inserting and querying time entries", () => {
    let connection;
    let db;
    const collectionName = process.env.COLLECTION_TEST || "time_entries";

    beforeAll(async () => {
        try {
            connection = await MongoClient.connect(process.env.DB_URI, {
                useNewUrlParser: true,
            });
            db = await connection.db();
            //Clear the content of the collection
            await db.collection(collectionName).deleteMany({});
        } catch (e) {
            console.log(e);
        }
    });

    afterAll(async () => {
        await connection.close();
    });

    test("Can acces Dotm data", async () => {
        const collections = await db.listCollections().toArray();
        //console.log(collections);
        const collectionNames = collections.map((elem) => elem.name);
        expect(collectionNames).toContain(collectionName);
    });

    test("Insert a new time entry for a MongoDB project", async () => {
        const project = "mongodb";
        const date = "1/12/2022";
        const time = 270;
        const description = "Working on #mongoDB for #woa course";

        await addTimeEntry(
            db.collection(collectionName),
            `${date}, ${time}`,
            project,
            description
        );

        const mockTimeEntry = setData(project, date, time, description);

        const insertedtimeEntry = await db.collection(collectionName).findOne({
            project: project,
        });
        console.log(insertedtimeEntry);
        expect(insertedtimeEntry.time_entries[0]).toEqual(
            mockTimeEntry.time_entries[0]
        );
    });

    test("Insert a new time entry on same project", async () => {
        const project = "mongodb";
        const date = "1/15/2022";
        const time = 60;
        const description =
            "Working on #mongoDB and setup the #gitlab repository";

        await addTimeEntry(
            db.collection(collectionName),
            `${date}, ${time}`,
            project,
            description
        );

        const mockTimeEntry = setData(project, date, time, description);

        const insertedtimeEntry = await db.collection(collectionName).findOne({
            project: project,
        });
        let nbDocs = await db.collection(collectionName).find().count();
        //Verify that the the new time entry inserted on the same document
        expect(nbDocs).toBe(1);
        expect(insertedtimeEntry.time_entries.length).toBe(2);
        expect(insertedtimeEntry.time_entries[1]).toEqual(
            mockTimeEntry.time_entries[0]
        );
    });

    test("Insert one time entry on a new VueJSproject", async () => {
        const project = "vueJS";
        const date = "1/15/2022";
        const time = "2pm-4pm";
        const description =
            "Working on #vuejs to program a pokedex to see #pokemons . Also part of a #woa project.";

        await addTimeEntry(
            db.collection(collectionName),
            `${date}, ${time}`,
            project,
            description
        );

        const mockTimeEntry = setData(
            project,
            date,
            basicPattern(time), //Use the basic pattern function to convert hourPattern
            description
        );

        const insertedtimeEntry = await db.collection(collectionName).findOne({
            project: project,
        });
        let nbDocs = await db.collection(collectionName).find().count();

        //Verify there are now 2 documents on the DB
        expect(nbDocs).toBe(2);
        expect(insertedtimeEntry.time_entries.length).toBe(1);
        expect(insertedtimeEntry.time_entries[0]).toEqual(
            mockTimeEntry.time_entries[0]
        );

        //Verify the pattern for hour was correctly converted

        expect(insertedtimeEntry.time_entries[0].time).toBe(120);
    });

    test("Insert another time entry on VueJS", async () => {
        const project = "vueJS";
        const date = "1/15/2022";
        const time = "2.5";
        const description = "Still working about #pokemons on #vuejs";

        await addTimeEntry(
            db.collection(collectionName),
            `${date}, ${time}`,
            project,
            description
        );

        const mockTimeEntry = setData(
            project,
            date,
            basicPattern(time),
            description
        );

        const insertedtimeEntry = await db.collection(collectionName).findOne({
            project: project,
        });
        let nbDocs = await db.collection(collectionName).find().count();

        //Verify there are now 2 documents on the DB
        expect(nbDocs).toBe(2);
        expect(insertedtimeEntry.time_entries.length).toBe(2);
        expect(insertedtimeEntry.time_entries[1]).toEqual(
            mockTimeEntry.time_entries[0]
        );

        //Verify the pattern for hour was correctly converted

        expect(insertedtimeEntry.time_entries[1].time).toBe(150);
    });

    test("Sumup all the data for one on vueJS projec", async () => {
        const project = "vueJS";
        const date = "1/15/2022";
        const results = await getProject(
            db.collection(collectionName),
            date,
            project
        );
        //Work 2h + 2,5h on the same day
        expect(results).toBe("4.5h");
    });

    test("Sumup all the data for both projects", async () => {
        const date = "1/15/2022";
        const results = await getProject(db.collection(collectionName), date);
        //Work 2h + 2,5h on vueJS
        //Work 1h on MongoDB
        expect(results).toContainEqual({ project: "mongodb", timeTotal: "1h" });
        expect(results).toContainEqual({ project: "vueJS", timeTotal: "4.5h" });
    });

    test("Sumup on the hashtag #pokemons on vueJS", async () => {
        const project = "vueJS";
        const hashtag = "#pokemons";
        const results = await getHash(
            db.collection(collectionName),
            hashtag,
            project
        );
        expect(results).toBe("4.5h");
    });

    test("Sumup on the hashtag #woa on both project", async () => {
        const hashtag = "#woa";
        const results = await getHash(db.collection(collectionName), hashtag);
        //Work 4.5h on Mongo with #woa
        //Work 2h on VueJS with #woa
        expect(results).toBe("6.5h");
    });

    test("Sumup the report per day on MongoDB", async () => {
        const project = "mongodb";
        const results = await getReportPerDay(
            db.collection(collectionName),
            project
        );
        expect(results).toContainEqual({
            date: "1/12/2022",
            timeTotal: "4.5h",
        });

        expect(results).toContainEqual({ date: "1/15/2022", timeTotal: "1h" });
    });
});
